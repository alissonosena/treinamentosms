﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entidades
{
    public class Medicamento : Entidade
    {
        public Medicamento(string nome, string composicao, string fabricante, bool generico, string unidadeMedida)
        {
            Nome = nome;
            Composicao = composicao;
            Fabricante = fabricante;
            Generico = generico;
            UnidadeMedida = unidadeMedida;
        }

        public string Nome { get; private set; }
        public string Composicao { get; private set; }
        public string Fabricante { get; private set; }
        public bool Generico { get; private set; }
        public string UnidadeMedida { get; private set; }



        

    }
}
