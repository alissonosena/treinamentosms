﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entidades
{
    public abstract class Entidade
    {
        public long Id { get; protected set; }
        public bool Ativo { get; protected set; }
        public bool Excluido { get; protected set; }
        public void Excluir() => Excluido = true;

    }
}
