﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Commands.Medicamento
{
    public class RegistrarMedicamentoCommand : ICommand
    {

        public string Nome { get; set; }
        public string Composicao { get; set; }
        public string Fabricante { get; set; }
        public bool Generico { get; set; }
        public string UnidadeMedida { get; set; }
        public double Preco { get; set; }

    }
}
