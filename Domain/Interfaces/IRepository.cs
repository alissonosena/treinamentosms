﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IRepository<TEntity> 
    {
        void Adicionar(TEntity obj);
        TEntity SelecionarPeloId(long id);
        IEnumerable<TEntity> SelecionarTodos();
        void Atualizar(TEntity obj);
        void Excluir(long id);
       
    }
}
