﻿using System.Threading;
using System.Threading.Tasks;
using Domain.Commands.Medicamento;
using Domain.Entidades;
using Domain.Interfaces;
using MediatR;

namespace Domain.CommandHandler
{
    public class MedicamentoCommandHandler : IRequestHandler<RegistrarMedicamentoCommand, RegistrarMedicamentoCommand>
    {
        private readonly IMedicamentoRepository _medicamentoRepository;
        private readonly IMediatorHandler _mediator;

        public MedicamentoCommandHandler(IMedicamentoRepository medicamentoRepository,
            IMediatorHandler mediator) 
        {
            _medicamentoRepository = medicamentoRepository;
            _mediator = mediator;
        }

        public Task<bool> Handle(RegistrarMedicamentoCommand request, CancellationToken cancellationToken)
        {
            return true;
        }
    }

}
