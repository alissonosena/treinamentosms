﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Commands;

namespace Domain.CommandHandler
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T command) where T : ICommand;
    }
}
