﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.ViewModels;
using Domain.CommandHandler;
using Domain.Commands.Medicamento;
using Domain.Entidades;
using Domain.Interfaces;

namespace Application.Services
{
    public class MedicamentoService : IMedicamentoService
    {
        private readonly IMedicamentoRepository _medicamentoRepository;
        private readonly IMediatorHandler _mediatorHandler;

        public MedicamentoService(IMedicamentoRepository medicamentoRepository, IMediatorHandler mediatorHandler)
        {
            _medicamentoRepository = medicamentoRepository;
            _mediatorHandler = mediatorHandler;
        }

        public void Atualizar(MedicamentoVM medicamentoVM)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Excluir(long id)
        {
            var registerCommand = new RegistrarMedicamentoCommand();
            _mediatorHandler.SendCommand(registerCommand);
        }

        public void RegistrarMedicamento(MedicamentoVM medicamentoVM)
        {
            var registerCommand = new RegistrarMedicamentoCommand();
            _mediatorHandler.SendCommand(registerCommand);
        }

        public MedicamentoVM SelecionarPeloId(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<MedicamentoVM> SelecionarTodos()
        {
            throw new NotImplementedException();
        }
    }
}
