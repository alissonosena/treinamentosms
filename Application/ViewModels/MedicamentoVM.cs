﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.ViewModels
{
    public class MedicamentoVM
    {
        [Key]
        public long Id { get; set; }

        [Required(ErrorMessage = "O nome é obrigatório.")]
        [MinLength(2)]
        [MaxLength(100)]
        [DisplayName("Nome")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "A composição é obrigatória.")]
        [MinLength(2)]
        [MaxLength(200)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "O fabricamente é obrigatório.")]
        [MinLength(2)]
        [MaxLength(50)]
        [DisplayName("Fabricante")]
        public string Fabricante { get; set; }

        [Required(ErrorMessage = "Informe se o medicamento é genérico.")]
        [DisplayName("Genérico")]
        public bool Generico { get; set; }

        [Required(ErrorMessage = "A unidade de medida é obrigatória.")]
        [MinLength(2)]
        [MaxLength(15)]
        [DisplayName("Unidade de Medida")]
        public string UnidadeMedida { get; set; }
    }
}
