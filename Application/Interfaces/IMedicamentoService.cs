﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.ViewModels;

namespace Application.Interfaces
{
    public interface IMedicamentoService : IDisposable
    {
        void RegistrarMedicamento(MedicamentoVM medicamentoVM);
        IEnumerable<MedicamentoVM> SelecionarTodos();
        MedicamentoVM SelecionarPeloId(long id);
        void Atualizar(MedicamentoVM medicamentoVM);
        void Excluir(long id);

    }
}
