﻿using Domain.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infra.Mapping
{
    public class MedicamentoMap : EntityTypeConfiguration<Medicamento>
    {
        public MedicamentoMap()
        {
            ToTable("Medicamento");

            HasKey(p => p.Id);

            Property(c => c.Nome)
                .HasColumnName("Nome")
                .IsRequired()
                .HasMaxLength(100);

            

            Property(c => c.Fabricante)
                .HasColumnName("Fabricante")
                .IsRequired();

            Property(c => c.UnidadeMedida)
                .HasColumnName("UnidadeMedida")
                .IsRequired();

            Property(c => c.Generico)
                .HasColumnName("Generico")
                .IsRequired();

            Property(c => c.Ativo)
                .HasColumnName("Ativo")
                .IsRequired();

        }

    }
}
