﻿using Domain.Entidades;
using Infra.Mapping;
using System.Data.Entity;

namespace Infra.Context
{
    public class ConsultaContexto : DbContext
    {

        public ConsultaContexto() : base("name=STRSQL") { }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            modelBuilder.Configurations.Add(new MedicamentoMap());
        }

        public DbSet<Medicamento> Medicamento { get; set; }


    }
}
