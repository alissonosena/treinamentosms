﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entidades;
using Domain.Interfaces;
using Infra.Context;

namespace Infra.Repository
{
    public class MedicamentoRepository : Repository<Medicamento>, IMedicamentoRepository
    {

        public MedicamentoRepository(ConsultaContexto context)
            : base(context)
        {

        }

        public Medicamento SelecionarMedicamentoPeloNome(string nome)
        {
            return SelecionarTodos().FirstOrDefault(c => c.Nome == nome);
        }


    }
}
