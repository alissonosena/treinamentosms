﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entidades;
using Infra.Context;

namespace Infra.Repository
{
    public class Repository<T> : IRepository<T> where T : Entidade
    {
        protected readonly ConsultaContexto _context;

        public Repository(ConsultaContexto context)
        {
            _context = context;
        }

        public void Adicionar(T obj)
        {
            _context.Set<T>().Add(obj);
            _context.SaveChanges();
        }

        public void Atualizar(T obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Excluir(long id)
        {
            var entity = SelecionarPeloId(id);
            entity.Excluir();
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }

      

        public T SelecionarPeloId(long id)
        {
            return _context.Set<T>().Find(id);
            
        }

        public IEnumerable<T> SelecionarTodos()
        {
            return _context.Set<T>().ToList();
        }
    }
}
