﻿using Application.Interfaces;
using Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class MedicamentoController : Controller
    {
        private readonly IMedicamentoService _medicamentoService;

        public MedicamentoController(IMedicamentoService medicamentoService)
        {
            _medicamentoService = medicamentoService;
        }

        public ActionResult RegistrarMedicamento()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RegistrarMedicamento(MedicamentoVM model)
        {
            if (ModelState.IsValid)
            {
                _medicamentoService.RegistrarMedicamento(model);
            }
            else
            {

                return "";
            }

            return View();
        }

    }
}